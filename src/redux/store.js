import {
  legacy_createStore as createStore,
  compose,
  applyMiddleware,
} from "redux";
import thunk from "redux-thunk";
import usersReducer from "./users/usersReducer";

const store = createStore(usersReducer, compose(applyMiddleware(thunk)));

export default store;
