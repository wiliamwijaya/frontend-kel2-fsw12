import axios from "axios";

export const getProductsAPI = async () => {
  // console.log(localStorage.getItem("access_token"));
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/products?limit=10&offset=0`
  );
  return res;
};
export const getDetailProductAPI = async (id) => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/products/${id}`
  );
  return res;
};

export const transactionProductAPI = async (id) => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/transaction/${id}`
  );
  return res;
};

// export const fetchCategoryAPI =({category})=>{
//   return(dispatch)=>{
//     dispatch(fetchCategoryAPI);
//     fetch(`https://fsw12-kel2-backend.herokuapp.com/api/v1/products?limit=10&offset=0&category=${category}`)
//       .then((res)=>{
//         if (res.status !==200){
//           throw Error ("Fetch Failed");
//         }
//         return res.json();
//       })
//       .then((category)=>{
//         console.log(category)
//       })
//   }
// }
export const getFilterProductAPI = async (category) => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/products?limit=20&offset=0&category=${category}`
  );
  return res;
};

export const getSearchProductAPI = async (search) => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/products?limit=10&offset=0&product_name=${search}`
  );
  return res;
};

export const postProductAPI = async ({ iduser, data }) => {
  const res = await axios.post(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/products`,
    data,
    {
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("access_token")}`,
      },
    }
  );
  return res;
};

export const getDaftarJualAPI = async (id) => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/products/sales/${id}`
  );
  return res;
};
export const getDiminatiAPI = async (id) => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/products/sales/${id}?favourite=true`
  );
  return res;
};
export const getTerjualAPI = async (id) => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/products/sales/${id}?status=true`
  );
  return res;
};

export const bidderApi = async ({ id, data }) => {
  const res = await axios.post(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/bidder/${id}`,
    data,
    {
      headers: {
        "content-type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("access_token")}`,
      },
    }
  );
  return res;
};
// axios({
//   method: 'get',
//   url: 'http://bit.ly/2mTM3nY',
//   responseType: 'stream'
// })
//   .then(function (response) {
//     response.data.pipe(fs.createWriteStream('ada_lovelace.jpg'))
//   });
