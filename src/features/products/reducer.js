import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  getProductsAPI,
  getDetailProductAPI,
  getFilterProductAPI,
  getSearchProductAPI,
  getDaftarJualAPI,
  getDiminatiAPI,
  getTerjualAPI,
  postProductAPI,
  transactionProductAPI,
  bidderApi,
} from "./reducerAPI";

const initialState = {
  show: {},
  data: [],
  isLoading: false,
  error: "",
  success: "",
  status: "idle",
  statusCreate: true,
};

export const getProducts = createAsyncThunk(
  "products/getProducts",
  async () => {
    const response = await getProductsAPI();
    // console.log(response);
    return response.data;
  }
);
export const getDetailProduct = createAsyncThunk(
  "products/getDetailProducts",
  async (id) => {
    const response = await getDetailProductAPI(id);
    return response.data;
  }
);
export const fetchCategory = createAsyncThunk(
  "products/fetchCategory",
  async (category) => {
    const response = await getFilterProductAPI(category);
    console.log(category);
    // console.log(response.category);
    return response.data;
  }
);

export const searchProduct = createAsyncThunk(
  "products/searchProduct",
  async (search) => {
    const response = await getSearchProductAPI(search);
    console.log(response.data);
    return response.data;
  }
);

export const postProduct = createAsyncThunk(
  "products/postProduct",
  async ({ iduser, data }) => {
    console.log(iduser);
    console.log(data);
    const response = await postProductAPI({
      iduser,
      data,
    });
    return response.data;
  }
);

export const getDaftarJual = createAsyncThunk(
  "products/getDaftarJual",
  async (id) => {
    const response = await getDaftarJualAPI(id);
    return response.data;
  }
);
export const getDiminati = createAsyncThunk(
  "products/getDiminati",
  async (id) => {
    const response = await getDiminatiAPI(id);
    return response.data;
  }
);

export const getTerjual = createAsyncThunk(
  "products/getTerjual",
  async (id) => {
    const response = await getTerjualAPI(id);
    return response.data;
  }
);

export const transactionProduct = createAsyncThunk(
  "products/transactionProduct",
  async (id) => {
    const response = await transactionProductAPI(id);
    return response.data;
  }
);

export const bidPrice = createAsyncThunk(
  "products/bidPrice",
  async ({ id, data }) => {
    const response = await bidderApi({ id, data });
    return response.data;
  }
);
// export const productsSlice = createSlice({
//   name: "products",
//   initialState,
//   reducers: {
//     setProducts: (state, action) => {
//       state.products = action.payload;
//     },
//     setProduct: (state, action) => {
//       state.products = action.payload;
//     },
//   },
// });

export const productsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    setUsers: (state, action) => {
      state.users = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getProducts.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getProducts.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      })
      .addCase(getDetailProduct.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getDetailProduct.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      })
      .addCase(fetchCategory.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchCategory.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      })
      .addCase(searchProduct.pending, (state) => {
        state.status = "loading";
      })
      .addCase(searchProduct.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      })
      .addCase(postProduct.pending, (state) => {
        return {
          ...state,
          loading: true,
        };
      })
      .addCase(postProduct.fulfilled, (state, action) => {
        return {
          ...state,
          loading: false,
          statusCreate: false,
        };
      })
      .addCase(postProduct.rejected, (state, action) => {
        return {
          ...state,
          loading: false,
          error: action.error.message,
          statusCreate: true,
        };
      })
      .addCase(getDaftarJual.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getDaftarJual.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      })
      .addCase(getDiminati.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getDiminati.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      })
      .addCase(getTerjual.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getTerjual.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      })
      .addCase(transactionProduct.pending, (state) => {
        state.status = "loading";
      })
      .addCase(transactionProduct.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      });
  },
});

export const { setUsers, setError } = productsSlice.actions;

export const data = (state) => state.products;

export default productsSlice.reducer;
