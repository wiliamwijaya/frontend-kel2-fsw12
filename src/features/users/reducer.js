import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  loginAPI,
  registerAPI,
  logoutUsers,
  listUserApi,
  updateApi,
  getUserApi,
  // usersLogin,
  // getUser,
  // updateUsers,
  // deleteUsers,
  // createUsers
} from "./reducerAPI";

const initialState = {
  users: [],
  status: false,
  loading: false,
  error: null,
};

export const listUser = createAsyncThunk("users/getusers", async () => {
  // console.log(localStorage.getItem("access_token"));
  const response = await listUserApi();
  console.log(response);
  return response.data;
});

export const usersLogin = createAsyncThunk("users/login", async (data) => {
  const response = await loginAPI(data);
  return response.data;
});

export const usersRegister = createAsyncThunk(
  "users/register",
  async (data) => {
    const response = await registerAPI(data);
    // console.log(data);
    return response.data;
  }
);

export const usersUpdate = createAsyncThunk(
  "users/update",
  async ({ iduser, data }) => {
    console.log(data);
    console.log(iduser);
    const response = await updateApi({
      iduser,
      data,
    });

    return response.data;
  }
);

export const usersLogout = createAsyncThunk("users/logoutUser", async (id) => {
  const response = await logoutUsers();
  return response;
});

export const getUsers = createAsyncThunk("users/getUser", async (iduser) => {
  const response = await getUserApi(iduser);
  return response;
});

// export const updateUsersAsync = createAsyncThunk(
//   "users/updateUsers",
//   async (users) => {
//     const updatedUsers = await updateUsers(users);
//     return updatedUsers;
//   }
// );

// export const deleteUsersAsync = createAsyncThunk(
//   "users/deleteUsers",
//   async (users) => {
//     const deletedUsers = await deleteUsers(users);
//     return deletedUsers;
//   }
// );

// export const createUsersAsync = createAsyncThunk(
//   "users/createUsers",
//   async (users) => {
//     const createdUsers = await createUsers(users);
//     return createdUsers;
//   }
// );

export const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    setUsers: (state, action) => {
      state.users = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
  },
  extraReducers: {
    [listUser.pending]: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    [listUser.fulfilled]: (state, action) => {
      return {
        ...state,
        loading: false,
        status: true,
      };
    },
    [listUser.rejected]: (state, action) => {
      return {
        ...state,
        loading: false,
        error: action.error.message,
        status: false,
      };
    },
    [usersLogin.pending]: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    [usersLogin.fulfilled]: (state, action) => {
      return {
        ...state,
        loading: false,
        status: true,
      };
    },
    [usersLogin.rejected]: (state, action) => {
      return {
        ...state,
        loading: false,
        error: action.error.message,
        status: false,
      };
    },
    [usersRegister.pending]: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    [usersRegister.fulfilled]: (state, action) => {
      return {
        ...state,
        loading: false,
        statusRegister: true,
      };
    },
    [usersRegister.rejected]: (state, action) => {
      return {
        ...state,
        loading: false,
        error: action.error.message,
        statusRegister: false,
      };
    },
    [usersUpdate.pending]: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    [usersUpdate.fulfilled]: (state, action) => {
      return {
        ...state,
        loading: false,
        statusUpdate: true,
      };
    },
    [usersUpdate.rejected]: (state, action) => {
      return {
        ...state,
        loading: false,
        error: action.error.message,
        statusUpdate: false,
      };
    },
    [usersLogout.pending]: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    [usersLogout.fulfilled]: (state, action) => {
      return {
        ...state,
        loading: false,
        statusLogout: true,
      };
    },
    [usersLogout.rejected]: (state, action) => {
      return {
        ...state,
        loading: false,
        error: action.error.message,
        statusLogout: false,
      };
    },
    [getUsers.pending]: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    [getUsers.fulfilled]: (state, action) => {
      return {
        ...state,
        loading: false,
        statusGet: true,
      };
    },
    [getUsers.rejected]: (state, action) => {
      return {
        ...state,
        loading: false,
        error: action.error.message,
        statusGet: false,
      };
    },
  },
});

export const { setUsers, setError } = usersSlice.actions;

export const users = (state) => state.users;

export default usersSlice.reducer;
