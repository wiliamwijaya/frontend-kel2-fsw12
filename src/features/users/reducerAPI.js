import axios from "axios";
// const tkn = localStorage.getItem("token");
// const token = localStorage.getItem('access_token');

export const listUserApi = async () => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/user`,
    {
      headers: {
        "content-type": "application/json",
        Authorization: "Bearer" + `${localStorage.getItem("access_token")}`,
      },
    }
  );
  return res;
};

export const getUserApi = async (iduser) => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/users/profile/${iduser}`,
    {
      headers: {
        "content-type": "application/json",
        Authorization: "Bearer" + `${localStorage.getItem("access_token")}`,
      },
    }
  );
  return res;
};

export const loginAPI = async (data) => {
  const res = await axios.post(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/users/login`,
    data,
    {
      headers: {
        Authorization: "Bearer" + localStorage.getItem("access_token"),
      },
    }
  );
  localStorage.setItem("access_token", res.data.token);
  return res;
};
export const registerAPI = async (data) => {
  const res = await axios.post(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/users/register`,
    data,
    {
      headers: {
        Authorization: "Bearer" + localStorage.getItem("access_token"),
      },
    }
  );
  return res;
};

export const updateApi = async ({ iduser, data }) => {
  const res = await axios.put(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/users/${iduser}`,
    data,
    {
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("access_token")}`,
      },
    }
  );
  return res;
};

export const logoutUsers = async () => {
  const res = await axios.get(
    `https://fsw12-kel2-backend.herokuapp.com/api/v1/users/logout`
  );
  return res;
};

// export const AuthLogin = createAsyncThunk('users/AuthLogin',
//   async (data) => {
//     // console.log(data)
//     const res = await AuthLogin.post('/login', data, {
//       H: {
//         'accept': 'application/json',
//         'Authorization': 'Bearer' + localStorage.getItem('token')
//       }
//     });
//     // console.log(res)
//     if (res.data.status) {
//       // console.log(res.data.data);
//       localStorage.setItem('token', res.data.data.token);
//       // localStorage.setItem('uid', res.data.data.user_id);
//       return res.data;
//     } else {
//       throw new Error(res.data.message);
//     }
//   });
// async () => {
// const users = await AuthLogin();
// return users;
// });

// export const getUser = async (id) => {
//   const res = await axios.get(
//     `https://my-json-server.typicode.com/Muhammadfmwp/db_json_finalbinar/user/${id}`
//   );
//   return res;
// };
// export const updateUsers = async (users) => {
//   const res = await axios.put(
//     `https://my-json-server.typicode.com/Muhammadfmwp/db_json_finalbinar/user`,
//     users
//   );
//   return res;
// };

// export const deleteUsers = async (users) => {
//   const res = await axios.delete(
//     `https://my-json-server.typicode.com/Muhammadfmwp/db_json_finalbinar/user`, {
//       data: users
//     }
//   );
//   return res;
// };

// export const createUsers = async (users) => {
//   const res = await axios.post(
//     `https://my-json-server.typicode.com/Muhammadfmwp/db_json_finalbinar/user`,
//     users
//   );
//   return res;
// };
