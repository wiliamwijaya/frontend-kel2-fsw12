import "./App.css";
import React from "react";
import { Routes, Route } from "react-router-dom";
import Landingpage from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import InfoProfile from "./pages/InfoProfil";
import InfoProduk from "./pages/BuatProduk";
import AkunProfil from "./pages/AkunProfil";
import Buyer from "./pages/Detailproduk";
import SellerProduct from "./pages/SellerProduct";
import InfoPenawar from "./pages/InfoPenawar";
import Chat from "./pages/Chat";
import io from "socket.io-client";
import { createTheme, ThemeProvider } from "@mui/material";

const socket = io.connect(process.env.REACT_APP_BACKEND_URL);

function App() {
  const theme = createTheme({
    typography: {
      button: {
        textTransform: "none",
        fontFamily: ["Poppins", "sans-serif"].join(","),
      },
    },
  });

  const menu = [
    { path: "/", component: <Landingpage /> },
    { path: "/login", component: <Login /> },
    { path: "/register", component: <Register /> },
    { path: "/infoprofile", component: <InfoProfile /> },
    { path: "/infoproduk", component: <InfoProduk /> },
    { path: "/akunprofil", component: <AkunProfil /> },
    { path: "/buyerproduk/:id", component: <Buyer /> },
    { path: "/sellerproduct/:id", component: <SellerProduct /> },
    {
      path: "/chat",
      component: <Chat />,
    },
    {
      path: "/infopenawar/:id",
      component: <InfoPenawar />,
    },
  ];

  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Routes>
          {menu.map((row, i) => (
            <Route
              key={i}
              exact
              path={row.path}
              element={row.component}
            ></Route>
          ))}
        </Routes>
      </ThemeProvider>
    </div>
  );
}

export default App;
