import * as React from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Grid,
  Box,
  Stack,
  Button,
  imageListItemBarClasses,
} from "@mui/material";
import { decodeToken } from "react-jwt";

import ".././css/Login.css";

import { Form } from "react-bootstrap";

import { styled } from "@mui/material/styles";

import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import AddIcon from "@mui/icons-material/Add";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { postProduct, setError, data } from "../features/products/reducer";
import { useEffect } from "react";
let token = localStorage.getItem("access_token");

const BuatProduk = () => {
  const [productName, setProductName] = React.useState("");
  const [price, setPrice] = React.useState("");
  const [category, setCategory] = React.useState("");
  const [description, setDescription] = React.useState("");
  // const [file, setFile] = React.useState(null);
  const [image, setImage] = React.useState("");
  const navigate = useNavigate();
  // const [setStatus] = useState();
  const postStatus = useSelector((state) => state.products.statusCreate);
  const dispatch = useDispatch();
 
  console.log(postStatus)
console.log(image.name)

  useEffect(() => {
    if (localStorage.getItem("access_token") === null) {
      alert("Harus Login Terlebih Dahulu!")
       navigate("/login");
    }
  })
  
  useEffect(() => {
    if (postStatus !== true) {
      navigate("/");
      // console.log(name);
    }
  }, [postStatus, navigate]);
  useEffect(() => {
    if (postStatus !== true) {
      alert("Create Product berhasil");
      window.location.reload();
      // console.log(name);
    }
  }, [postStatus]);

  const Input = styled("input")({
    display: "none",
  });

  


  const handleSubmit = (e) => {
    if (productName === "" || price === "" || category === "" || description === "" || image === "") {
      dispatch(setError("Isi semua form dengan lengkap!"));}
    else{
    const id = decodeToken(token);
    let iduser = id.id;

    // e.preventDefault();
    // console.log(productName);
    // console.log(price)
          const data = new FormData();
          data.append("productName", productName);
          data.append("price", price);
          data.append("category", category);
          data.append("description", description);
          data.append("image", image)
          dispatch(postProduct({iduser,data}));
          // console.log(iduser,data);
    }
  };
  return (
    <div role="info-produk">
      <Grid
        container
        component="main"
        sx={{ height: "100vh", overflowX: "hidden" }}
      >
        <AppBar
          position="absolute"
          sx={{
            position: "relative",
            backgroundColor: "white",
            alignItems: "center",
          }}
        >
          <Toolbar>
            <IconButton
              edge="start"
              aria-label="arrow back"
              className="arroproduk"
              onClick={() => {
                navigate("/");
              }}
            >
              <ArrowBackIcon />
            </IconButton>
            <Typography
              className="titleprofile"
              variant="subtitle2"
              component="div"
              color="black"
            >
              Lengkapi Info Detail Produk
            </Typography>
          </Toolbar>
        </AppBar>

        <Grid container sx={{ height: "100vh" }}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Box
              className="profil-box"
              sx={{
                mt: 2,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Stack direction="row" spacing={30}>
                <IconButton
                  aria-label="arrow back"
                  sx={{ ml: "-300px" }}
                  onClick={() => {
                    navigate("/");
                  }}
                >
                  <ArrowBackIcon />
                </IconButton>
              </Stack>

              <Box
                noValidate
                // onSubmit={handleSubmit}
                sx={{ width: 400, height: 400 }}
              >
                <div className="forminfo">
                  <Form className="form-group mb-4">
                    <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlInput1"
                    >
                      <Form.Label>Nama Produk</Form.Label>
                      <Form.Control
                        type="name"
                        placeholder="Nama Produk"
                        style={{
                          borderRadius: "16px",
                          fontSize: "14px",
                          fontFamily: "Poppins",
                        }}
                        value={productName}
                        onChange={(e) => setProductName(e.target.value)}
                      />
                    </Form.Group>

                    <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlInput1"
                    >
                      <Form.Label>Harga Produk</Form.Label>
                      <Form.Control
                        type="name"
                        placeholder="Harga Produk"
                        style={{
                          borderRadius: "16px",
                          fontSize: "14px",
                          fontFamily: "Poppins",
                        }}
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3">
                      <Form.Label>Kategori</Form.Label>
                      <Form.Select
                        aria-label="Default select example"
                        style={{
                          borderRadius: "16px",
                          fontSize: "14px",
                          fontFamily: "Poppins",
                        }}
                        value={category}
                        onChange={(e) => setCategory(e.target.value)}
                      >
                        <option>Pilih Kategori</option>
                        <option value="hobi">Hobi</option>
                        <option value="accessories">Aksesoris</option>
                        <option value="kendaraan">Kendaraan</option>
                        <option value="baju">Baju</option>
                        <option value="elektronik">Elektronik</option>
                        <option value="elektronik">Kesehatan</option>
                      </Form.Select>
                    </Form.Group>

                    <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlTextarea1"
                    >
                      <Form.Label>Deskripsi</Form.Label>
                      <Form.Control
                        as="textarea"
                        rows={3}
                        placeholder="Contoh: Jalan Ikan Hiu 33"
                        style={{
                          borderRadius: "16px",
                          fontSize: "14px",
                          fontFamily: "Poppins",
                        }}
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3">
                      <Form.Label>Foto Produk</Form.Label>
                    </Form.Group>
                    <Box
                      component="span"
                      sx={{ p: 2, border: "1px dashed grey" }}
                    >
                      <label htmlFor="icon-button-file">
                      {image?<img src={image.name} alt={image.name}/>:  <Input
                          id="icon-button-file"
                          type="file"
                          onChange={(e) => setImage(e.target.files[0])}
                        />}
                        <IconButton
                          aria-label="upload picture"
                          component="span"
                        >
                          <AddIcon />
                        </IconButton>
                        <img src={image} alt=""/>
                      </label>
                    </Box>
                  </Form>

          
                  <Stack spacing={2} direction="row">
                    <Button
                      fullWidth
                      variant="outlined"
                      sx={{
                        borderRadius: "16px",
                        border: "1px solid  #7126B5",
                      }}
                      onClick={() => {
                        navigate("/buyerproduk");
                      }}
                    >
                      Preview
                    </Button>
                    <Button
                      fullWidth
                      variant="contained"
                      sx={{
                        backgroundColor: " #7126B5",
                        borderRadius: "16px",
                      }}
                      onClick={handleSubmit}
                    >
                      Terbitkan
                    </Button>
                  </Stack>

                  <br />
                </div>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default BuatProduk;
