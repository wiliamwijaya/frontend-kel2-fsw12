import { useEffect, useState } from 'react'
import logo from '../logo.svg'
import io from 'socket.io-client'
import '../App.css'
import ChatMessage from "../component/ChatMessage";
import { Room } from '@mui/icons-material';

const socket = io.connect(process.env.REACT_APP_BACKEND_URL);

function Chat () {
  const [message, setMessage] = useState('')
  const [messages, setMessages] = useState([])

  const [username, setUsername] = useState("");
  const [room, setRoom] = useState("");

  const joinRoom = () => {
    if (username !== "" && room !== "") {
      socket.emit("join_room", room);
    }
  };
  

  const sendMessage = async() => {
    if(message !== ""){
        const messageData = {
            room: room,
            author: username,
            message: message,
    
        }
    }
  }
  
  function handleTextChange (e) {
    setMessage(e.target.value)
    console.log(message)
  }

  function handleSubmit (e) {
    e.preventDefault()
    if (!message || message === '') return

    socket.emit('chat message', message)
    setMessages([...messages, { msg: message, status: 0, time: new Date() }])
    setMessage('')
  }

  useEffect(() => {
    socket.on('incoming message', (message) => {
      console.log(message)
      setMessages([...messages, { msg: message, status: 1 }])
    })
  }, [socket, messages])


  return (
    <div className='App'>
      <h3>Join Room</h3>
        <input
          type="text"
          placeholder="username"
          onChange={(e) => {
            setUsername(e.target.value);
          }}
        />
        <input
          type="text"
          placeholder="room id"
          onChange={(e) => {
            setRoom(e.target.value);
          }}
        />
        <button onClick={joinRoom}>Join Room</button>
        <ChatMessage socket={socket} username={username} room={room} />
    </div>
  )
}

export default Chat
