import React from "react";
import Navbar from "../component/Navbar";
import AddIcon from "@mui/icons-material/Add";
// import { useNavigate } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/Card.css";
import {
  Button,
  Box,
  Grid,
  Card,
  CardContent,
  Typography,
  CardMedia,
  Container,
  useMediaQuery,
} from "@mui/material";
import ".././css/Filter.css";
import ".././css/Carousel.css";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.min.css";
import "owl.carousel/dist/assets/owl.theme.default.min.css";
import { Row, Col } from "react-bootstrap";
import Navbar3Icon from "../component/Navbar3Icon";
import { Search } from "@mui/icons-material";
import { getProducts, data, fetchCategory } from "../features/products/reducer";
import { listUser, users } from "../features/users/reducer";
// let dat =  {data} from '../features/users/reducer';
import { useSelector, useDispatch } from "react-redux";
// import { fetchCategoryAPI } from "../features/products/reducerAPI";

const button = {
  "&.active": {
    background: `#7126B5`,
    borderRadius: `12px`,
    borderColor: "transparent",
    padding: "14px 24px",
    color: "#FFFFFF",
    marginRight: "16px",
  },
  background: `#E2D4F0;`,
  borderRadius: `12px`,
  borderColor: "transparent",
  padding: "14px 24px",
  color: "#3C3C3C",
  marginRight: "16px",
};

const responsivebutton = {
  "&.active": {
    background: `#7126B5`,
    borderRadius: `12px`,
    borderColor: "transparent",
    padding: "14px 60px",
    color: "#FFFFFF",
    marginRight: "16px",
  },
  background: `#E2D4F0;`,
  borderRadius: `12px`,
  borderColor: "transparent",
  padding: "14px 60px",
  color: "#3C3C3C",
  marginRight: "16px",
};

const isLogin = localStorage.getItem("access_token");
function Landingpage() {
  const navigate = useNavigate();
  // const [setStatus] = useState();
  const history = useNavigate();
  let posts = useSelector(data);
  console.log(localStorage.getItem("userInfo"));
  posts = posts?.data?.data?.products?.data;
  const dispatch = useDispatch();
  const fetchData = () => {
    dispatch(getProducts());
    // dispatch(listUser(token));
  };
  const asd = () => {
    dispatch(listUser);
  };
  useEffect(() => {
    fetchData();
    asd();
  }, []);
  // setTimeout(() =>{
  //     setStatus(null);
  // }, 3000);
  // const kategori = useSelector(data);
  //   const [items, setItems] = useState();
  //   const filterItem = (categItem) => {
  //     const updatedItems = posts.filter((category) => {
  //         return category.category === categItem;
  //     });

  //     setItems(updatedItems);
  // }

  const isMobile = useMediaQuery("(max-width:768px)");

  return (
    <div role="home" style={{ overflowX: "hidden" }}>
      {isLogin ? <Navbar3Icon /> : <Navbar />}

      <div role="carousel">
        <div className="hide">
          <img
            src={process.env.PUBLIC_URL + "/images/headerm.png"}
            style={{ width: "100%" }}
            alt=""
          />
          <img
            src={process.env.PUBLIC_URL + "/images/giftm.png"}
            style={{
              position: "absolute",
              left: "220px",
              top: "170px",
              width: "127px",
              height: "123px",
            }}
            alt=""
          />
        </div>

        <div className="padding">
          <OwlCarousel
            loop={true}
            autoplay={true}
            autoplayTimeout={5000}
            center={true}
            responsive={{
              0: {
                items: 1,
              },
              800: {
                items: 1,
              },
              1000: {
                items: 2,
              },
            }}
          >
            <Row>
              <Col className="slide-card">
                <img
                  src={process.env.PUBLIC_URL + "/images/img banner.png"}
                  alt=""
                />
              </Col>
            </Row>
            <Row>
              <Col className="slide-card">
                <img
                  src={process.env.PUBLIC_URL + "/images/img banner.png"}
                  alt=""
                />
              </Col>
            </Row>
          </OwlCarousel>
        </div>
      </div>

      <div role="filter">
        <Box className={"marginFilter"}>
          <Box>
            <Typography
              fontWeight={"700"}
              sx={{ textAlign: "left" }}
              fontSize="16px"
            >
              Telusuri Kategori
            </Typography>

            <Box py={1} sx={{ display: "flex" }} className={"wrap"}>
              <Button
                onClick={() => dispatch(getProducts())}
                variant="contained"
                sx={isMobile ? responsivebutton : button}
                className={"active"}
              >
                <Search />
                Semua
              </Button>
              <Button
                onClick={() => dispatch(fetchCategory("accessories"))}
                variant="contained"
                sx={isMobile ? responsivebutton : button}
              >
                <Search />
                Aksesoris
              </Button>
              <Button
                onClick={() => dispatch(fetchCategory("hobi"))}
                variant="contained"
                sx={isMobile ? responsivebutton : button}
              >
                <Search />
                Hobi
              </Button>
              <Button
                onClick={() => dispatch(fetchCategory("baju"))}
                variant="contained"
                sx={isMobile ? responsivebutton : button}
              >
                <Search />
                Baju
              </Button>
              <Button
                onClick={() => dispatch(fetchCategory("elektronik"))}
                variant="contained"
                sx={isMobile ? responsivebutton : button}
              >
                <Search />
                Elektronik
              </Button>
              <Button
                onClick={() => dispatch(fetchCategory("kesehatan"))}
                variant="contained"
                sx={isMobile ? responsivebutton : button}
              >
                <Search />
                Kesehatan
              </Button>
            </Box>
          </Box>
        </Box>
      </div>

      <div className="card-product">
        {posts && (
          <Container sx={{ border: "none" }}>
            <Grid container spacing={3}>
              {posts?.map((row, i) => (
                <Grid key={i} item xs={2}>
                  <Card
                    onClick={() => history(`/buyerproduk/${row.id}`)}
                    style={{ height: "auto" }}
                  >
                    <CardMedia
                      component="img"
                      src={row?.productImages?.[0].image}
                      style={{ height: "150px" }}
                    />
                    <CardContent>
                      <b>{row?.productName}</b>
                    </CardContent>
                    <CardContent>{row?.category}</CardContent>
                    <CardContent>Rp. {row?.price}</CardContent>
                  </Card>
                </Grid>
              ))}
              {posts.status === "loading" && <Typography>Loading</Typography>}
            </Grid>
          </Container>
        )}
      </div>
      <Box
        width={1}
        textAlign="center"
        sx={{
          position: "fixed",
          bottom: 46,
          left: 0,
        }}
      >
        <Button
          variant="contained"
          sx={{
            backgroundColor: "#7126B5",
            padding: "16px 24px",
            width: "115px",
            height: "52px",
            borderRadius: "12px",
            color: "white",
            marginRight: "10px",
            boxShadow: "0px 0px 10px #7126B5",
          }}
          onClick={() => {
            navigate(`/infoproduk`);
          }}
        >
          <AddIcon />
          Jual
        </Button>
      </Box>
    </div>
  );
}

export default Landingpage;
