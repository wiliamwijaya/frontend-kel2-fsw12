import * as React from "react";
import ".././css/Login.css";
import {
  CssBaseline,
  Link,
  Paper,
  Box,
  Grid,
  Typography,
  FormControl,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Tooltip,
} from "@mui/material";
import { Button } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';
import { Visibility, VisibilityOff } from "@mui/icons-material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import image from "../images/img.png";
import { usersLogin } from "../features/users/reducer";
const isLogin = localStorage.getItem('access_token');
export default function Login() {
  const [values, setValues] = React.useState({
    password: "",
    showPassword: false,
  });
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const status = useSelector((state) => state.users.status);
  const dispatch = useDispatch();
  const Navigate = useNavigate();
  
    const handlelogin = (e) => {
      e.preventDefault();
      if (email === "" || values.password === "") {
        alert("Email dan Password tidak boleh kosong");
      } else{
          const data = {
            email,
            password,
          };
          dispatch(usersLogin(data));
        }
      }
      useEffect(() => {
        if(isLogin){
          Navigate('/')
        }
      },[isLogin])
      useEffect(() => {
        if (status) {
          alert("Login Berhasil");
          window.location.reload();
      }
    }, [status, Navigate]);

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
    setPassword(event.target.value);
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <div role="login">
      <Grid
        container
        component="main"
        sx={{ height: "100vh", overflowX: "hidden" }}
      >
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={false}
          md={7}
          lg={6}
          sx={{
            backgroundImage: `url(${image})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />

        <Grid
          item
          xs={12}
          sm={12}
          md={5}
          lg={6}
          component={Paper}
          elevation={6}
          square
        >
          <Tooltip title="left" placement="left-end">
            <IconButton aria-label="arrow back" className="arrow-back">
              <ArrowBackIcon />
            </IconButton>
          </Tooltip>

          <Box
            className="login-box"
            sx={{
              px: 6,
              py: 12,

              display: "flex",
              flexDirection: "column",
            }}
          >
            <Typography
              variant="h6"
              gutterBottom
              component="div"
              fontWeight="bold"
            >
              Masuk
            </Typography>
            
            <Box
              component="form"
              noValidate
              sx={{ mt: 1 }}
            >
              <FormControl fullWidth sx={{ mb: 2 }}>
                <Typography variant="caption" display="block" gutterBottom>
                  Email
                </Typography>
                <OutlinedInput
                  id="email"
                  size="small"
                  name="email"
                  label="Email"
                  placeholder="Contoh: johndee@gmail.com"
                  onChange={(e) => setEmail(e.target.value)}
                  sx={{ borderRadius: "16px", fontSize: "14px" }}
                />
              </FormControl>

              <FormControl fullWidth sx={{ mb: 2 }}>
                <Typography variant="caption" display="block" gutterBottom>
                  Password
                </Typography>
                <OutlinedInput
                  id="password"
                  name="password"
                  type={values.showPassword ? "text" : "password"}
                  value={password}
                  size="small"
                  placeholder="Password"
                  onChange={handleChange("password")}
                  sx={{ borderRadius: "16px", fontSize: "14px" }}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {values.showPassword ? (
                          <VisibilityOff />
                        ) : (
                          <Visibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                  label="Password"
                />
              </FormControl>

              <div className="d-grid gap-2">
                <Button
                  size="lg"
                  style={{
                    marginBottom: "10px",
                    marginTop: "10px",
                    backgroundColor: "#7126B5",
                    borderRadius: "16px",
                    fontSize: "14px",
                    fontFamily: "Poppins",
                  }}
                  onClick={handlelogin}
                >
                  Masuk
                </Button>
              </div>

              <Grid container justifyContent="center" className="hrf">
                <Grid item>
                  <Typography variant="subtitle2" gutterBottom component="div">
                    Belum punya akun?
                    <Button variant="body2" onClick={() => Navigate("/register")}>
                   
                      Daftar di sini 
                    </Button>
                  </Typography>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}
