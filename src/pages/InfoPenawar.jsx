import * as React from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Grid,
  Stack,
  Box,
} from "@mui/material";
import { Card } from "react-bootstrap";

import { Container, Row, Col, Button, Modal, Form } from "react-bootstrap";
import { Icon } from "@mui/material";

import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";

import { transactionProduct, data } from "../features/products/reducer";

import { decodeToken } from "react-jwt";

import ArrowBackIcon from "@mui/icons-material/ArrowBack";

import "../css/InfoPenawar.css";

let token = localStorage.getItem("access_token");

const InfoPenawar = () => {
  const getId = decodeToken(token);
  let iduser = getId.id;
  console.log(iduser);

  const getUser = decodeToken(token);
  let user = getUser.name;
  console.log(user);

  const getImage = decodeToken(token);
  let imagepro = getImage.image;
  console.log(imagepro);

  const getCity = decodeToken(token);
  let citypro = getCity.city;
  console.log(citypro);

  const navigate = useNavigate();
  const [modalShow, setModalShow] = React.useState(false);

  const [statusButton, setStatusButton] = React.useState(true);

  const [status, setStatus] = React.useState(true);
  const [value, setValue] = React.useState(false);

  const { id } = useParams();
  const newtrans = useSelector(data);
  const trans = newtrans?.data?.data;

  console.log(trans);

  const dispatch = useDispatch();

  const fetchData = () => {
    dispatch(transactionProduct(id));
  };

  useEffect(() => {
    fetchData();
  }, []);

  // console.log(trans?.[0].Product?.productImages?.[0].image);

  return (
    <div>
      <Grid
        container
        component="main"
        sx={{ height: "100vh", overflowX: "hidden" }}
      >
        <AppBar
          position="absolute"
          sx={{
            position: "relative",
            backgroundColor: "white",
            alignItems: "center",
          }}
        >
          <Toolbar>
            <IconButton
              edge="start"
              aria-label="arrow back"
              className="arroproduk"
              onClick={() => {
                navigate("/");
              }}
            >
              <ArrowBackIcon />
            </IconButton>
            <Typography
              className="titleprofile"
              variant="subtitle2"
              component="div"
              color="black"
            >
              Info Penawar
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container sx={{ height: "100vh" }}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Box
              className="penawar-box"
              sx={{
                mt: 2,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Stack direction="row" spacing={30}>
                <IconButton
                  aria-label="arrow back"
                  sx={{ ml: "-300px" }}
                  onClick={() => {
                    navigate("/");
                  }}
                >
                  <ArrowBackIcon />
                </IconButton>
              </Stack>

              <Box
                sx={{
                  width: 400,
                  height: 400,
                }}
              >
                <div className={`d-flex justify-content-center`}>
                  <div style={{ maxWidth: "800px", margin: "0 50px" }}>
                    <Card className="d-flex carduser mt-3">
                      <div className="d-flex justify-content-left align-items-center w-100">
                        <img src={imagepro} alt="" className="me-3 img" />
                        <div>
                          <h5>{user}</h5>
                          <p className="m-0" style={{ fontSize: "14px" }}>
                            {citypro}
                          </p>
                        </div>
                      </div>
                    </Card>
                    <h6 className={`fw-bold my-4`}>
                      Daftar Produkmu yang Ditawar
                    </h6>
                    {trans && (
                      <>
                        <Container fluid>
                          <div
                            className="product-offer-list"
                            style={{ minWidth: "300px" }}
                          >
                            {trans?.map((row, i) => (
                              <Row key={i} style={{ marginTop: "20px" }}>
                                <Col
                                  xs={3}
                                  className="px-0"
                                  style={{ width: "80px", height: "80px" }}
                                >
                                  <img
                                    src={row?.Product?.productImages?.[0].image}
                                    alt=""
                                    className="productImage"
                                  />
                                </Col>

                                <Col>
                                  <div className="d-flex justify-content-between">
                                    <p
                                      className="text-secondary mb-2"
                                      style={{ fontSize: "14px" }}
                                    >
                                      Penawaran produk
                                    </p>
                                    <p
                                      className="text-secondary mb-2"
                                      style={{ fontSize: "14px" }}
                                    >
                                      {row?.Product?.createdAt}
                                    </p>
                                  </div>
                                  <h6 className="my-1">
                                    {row?.Product?.productName}
                                  </h6>
                                  <p className="my-1">
                                    Rp. {row?.Product?.price}
                                  </p>
                                  <h6 className="my-1">
                                    Ditawar Rp. {row?.bid}
                                  </h6>
                                </Col>

                                <Col
                                  xs={12}
                                  className="d-flex justify-content-end p-0 mt-3"
                                >
                                  {statusButton ? (
                                    <>
                                      <Button className="me-3 btnDecision">
                                        Tolak
                                      </Button>
                                      <Button
                                        className="me-3 btnDecision"
                                        onClick={() => {
                                          setStatusButton(false);
                                          setModalShow(true);
                                        }}
                                      >
                                        Terima
                                      </Button>
                                    </>
                                  ) : (
                                    <>
                                      <Button
                                        className="me-3 btnDecision"
                                        onClick={() => {
                                          setStatus(false);
                                          setModalShow(true);
                                        }}
                                      >
                                        Status
                                      </Button>

                                      <Button
                                        className="me-3 btnDecision"
                                        href="https://wa.me/6281234567890"
                                      >
                                        Hubungi di <Icon>whatsapp</Icon>
                                      </Button>
                                    </>
                                  )}
                                </Col>
                              </Row>
                            ))}
                          </div>
                        </Container>

                        <Modal
                          centered
                          aria-labelledby="contained-modal-title-vcenter"
                          dialogClassName="modal-size"
                          contentClassName="modal-style"
                          className="modal-centered"
                          show={modalShow}
                          onHide={() => {
                            setModalShow(false);
                          }}
                        >
                          <Modal.Header
                            closeButton
                            className="px-4 pt-3 border-0"
                          />
                          {status ? (
                            <Modal.Body className="mx-4 p-0">
                              <>
                                <h6>
                                  Yeay kamu berhasil mendapat harga yang sesuai
                                </h6>
                                <p className="text-secondary m-0">
                                  Segera hubungi pembeli melalui whatsapp untuk
                                  transaksi selanjutnya
                                </p>
                                <div className="mt-3 d-flex flex-column justify-content-center match-product-detail">
                                  <h6
                                    style={{
                                      textAlign: "center",
                                      marginBottom: "20px",
                                    }}
                                  >
                                    Product Match
                                  </h6>

                                  <Row className="mb-3">
                                    <Col xs="4">
                                      <img
                                        src={imagepro}
                                        style={{
                                          width: "100%",
                                          maxWidth: "max-content",
                                          height: "100%",
                                          minHeight: "75px",
                                          borderRadius: "12px",
                                          objectFit: "cover",
                                        }}
                                        alt=""
                                      />
                                    </Col>
                                    <Col className="ps-0 d-flex flex-column justify-content-center">
                                      <p
                                        className="mb-1"
                                        style={{
                                          fontSize: "14px",
                                          fontWeight: "500",
                                        }}
                                      >
                                        {user}
                                      </p>
                                      <p
                                        className="text-secondary my-0"
                                        style={{ fontSize: "14px" }}
                                      >
                                        {citypro}
                                      </p>
                                    </Col>
                                  </Row>

                                  <Row>
                                    <Col xs="4">
                                      <img
                                        src={
                                          trans?.[0].Product?.productImages?.[0]
                                            .image
                                        }
                                        style={{
                                          width: "100%",
                                          maxWidth: "max-content",
                                          height: "100%",
                                          minHeight: "75px",
                                          borderRadius: "12px",
                                          objectFit: "cover",
                                        }}
                                        alt=""
                                      />
                                    </Col>
                                    <Col className="ps-0 d-flex flex-column justify-content-center">
                                      <p
                                        className="mb-1"
                                        style={{ fontSize: "14px" }}
                                      >
                                        {trans?.[0].Product?.productName}
                                      </p>
                                      <p
                                        className="mb-1"
                                        style={{
                                          fontSize: "14px",
                                          textDecoration: "line-through",
                                        }}
                                      >
                                        {trans?.[0].Product?.price}
                                      </p>
                                      <p
                                        className="m-0"
                                        style={{ fontSize: "14px" }}
                                      >
                                        {trans?.[0].bid}
                                      </p>
                                    </Col>
                                  </Row>
                                </div>
                              </>
                            </Modal.Body>
                          ) : (
                            <Modal.Body className="mx-4 p-0">
                              <h6>Perbarui status penjualan produkmu</h6>
                              <Form.Group className="mt-4">
                                <Form.Check
                                  name="status"
                                  label="Berhasil terjual"
                                  type="radio"
                                  // checked={value === "Sold Out"}
                                  // onClick={(e) => {
                                  //   setValue("Sold Out");
                                  // }}
                                />
                                <Form.Label
                                  className="mb-4 ms-4 text-secondary"
                                  style={{ fontSize: "14px" }}
                                >
                                  Kamu telah sepakat menjual produk ini kepada
                                  pembeli
                                </Form.Label>
                                <Form.Check
                                  name="status"
                                  label="Batalkan transaksi"
                                  type="radio"
                                  // checked={value === "Cancel"}
                                  // onClick={(e) => {
                                  //   setValue("Cancel");
                                  // }}
                                />
                                <Form.Label
                                  className="ms-4 text-secondary"
                                  style={{ fontSize: "14px" }}
                                >
                                  Kamu membatalkan transaksi produk ini dengan
                                  pembeli
                                </Form.Label>
                              </Form.Group>
                            </Modal.Body>
                          )}

                          <Modal.Footer className="px-4 py-4 border-0">
                            <Button
                              className="m-0 modal-button-action"
                              onClick={(e) => {
                                setModalShow(false);
                                setStatusButton(false);
                              }}
                            >
                              {status ? "Hubungi via Whatsapp " : "Kirim"}
                            </Button>
                          </Modal.Footer>
                        </Modal>
                      </>
                    )}
                  </div>
                </div>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default InfoPenawar;
