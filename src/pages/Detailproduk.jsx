import React from "react";
import "../css/Buyer.css";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import {
  Card,
  Container,
  Row,
  Col,
  Carousel,
  Modal,
  Form,
} from "react-bootstrap";
import { Typography } from "@mui/material";
import Navbar from "../component/Navbar";
import Navbar3Icon from "../component/Navbar3Icon";
import {
  getDetailProduct,
  bidPrice,
  data,
  setError,
} from "../features/products/reducer";
import { useState } from "react";

const isLogin = localStorage.getItem("access_token");

function Buyer() {
  const [modalShow, setModalShow] = React.useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const { id } = useParams();
  const newdata = useSelector(data);
  const product = newdata.data.data;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const fetchData = () => {
    dispatch(getDetailProduct(id));
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    setIsLoggedIn(!!isLogin);
  }, [isLogin]);
  return (
    <>
      {isLogin ? <Navbar3Icon /> : <Navbar />}
      <Container className={`mt-5`}>
        {product && (
          <Row>
            <Col className={`mb-4`}>
              <Carousel>
                {product.products.productImages?.map((row, i) => (
                  <Carousel.Item key={i}>
                    <img
                      className="d-block w-100 image"
                      src={row.image}
                      alt=""
                    />
                  </Carousel.Item>
                ))}
              </Carousel>
              <Card className={`p-2 card mt-4`}>
                <Card.Body>
                  <h5 className={`mb-4`}>Deskripsi</h5>
                  <Typography>{product.products.description}</Typography>
                </Card.Body>
              </Card>
            </Col>
            <Col xl="4" lg="4" md="5" sm="12" xs="12">
              <Card className={`p-2`} style={{ borderRadius: "12px" }}>
                <Card.Body>
                  <h5>{product.products.productName}</h5>
                  <Card.Text
                    className={`m-0 text-secondary`}
                    style={{ fontSize: "14px" }}
                  >
                    {product.products.category}
                  </Card.Text>
                  <h6 className={`mt-3 mb-4`}>Rp. {product.products.price}</h6>
                  {!isLoggedIn ? (
                    <button
                      className={`button`}
                      onClick={() => navigate("/Login")}
                    >
                      Saya tertarik dan ingin nego
                    </button>
                  ) : (
                    <button
                      className={`button`}
                      onClick={() => setModalShow(true)}
                    >
                      Saya tertarik dan ingin nego
                    </button>
                  )}
                </Card.Body>
              </Card>
              <Card className="d-flex carduser mt-3">
                <div className="d-flex justify-content-left align-items-center w-100">
                  <img
                    src={product?.seller?.image}
                    alt=""
                    className="me-3 img"
                  />
                  <div>
                    <h5>{product?.seller?.name}</h5>
                    <p className="m-0" style={{ fontSize: "14px" }}>
                      {product?.seller?.city}
                    </p>
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
        )}
        <ModalPopUp
          show={modalShow}
          onHide={() => {
            setModalShow(false);
          }}
        />
      </Container>
    </>
  );
}

export default Buyer;

function ModalPopUp(props) {
  const { id } = useParams();
  const newdata = useSelector(data);
  const product = newdata.data.data;
  const dispatch = useDispatch();
  const [bid, setBid] = React.useState("");

  const handlerBidprice = (e) => {
    e.preventDefault();
    if (bid == "") {
      dispatch(setError("bid tidak boleh kosong!"));
    } else {
      const data = {
        bid,
      };
      dispatch(bidPrice({ id, data }));
      props.onHide();
    }
  };

  const fetchData = () => {
    dispatch(getDetailProduct(id));
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Modal {...props}>
      <Modal.Header closeButton className={`px-4 pt-3 border-0`} />
      <Modal.Body className={`mx-4 p-0`}>
        <h6>Masukkan Harga Tawarmu</h6>
        <p className={`text-secondary m-0`}>
          Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan
          segera dihubungi penjual.
        </p>
        <div className={`mt-3 d-flex flex-column justify-content-center`}>
          {product && (
            <Row>
              <Col xs="4">
                <img
                  style={{
                    width: "100%",
                    maxWidth: "max-content",
                    height: "100%",
                    minHeight: "75px",
                    borderRadius: "12px",
                    objectFit: "cover",
                  }}
                  src={product.products.productImages?.[0].image}
                  alt=""
                />
              </Col>

              <Col className={`ps-0 d-flex flex-column justify-content-center`}>
                <h6 className={`mb-1`}>{product.products.productName}</h6>
                <p
                  className={`text-secondary my-0`}
                  style={{ fontSize: "14px" }}
                >
                  Rp. {product.products.price}
                </p>
              </Col>
            </Row>
          )}
        </div>

        <div className={`mt-3`}>
          <p className={`m-0`} style={{ fontSize: "14px", fontWeight: "500" }}>
            Harga Tawar
          </p>
          <Form.Group controlId="price">
            <Form.Control
              type="number"
              style={{ borderRadius: "16px", height: "48px" }}
              placeholder="100000"
              pattern="^[0-9]+$"
              onChange={(e) => setBid(e.target.value)}
            />
          </Form.Group>
        </div>
      </Modal.Body>

      <Modal.Footer className={`px-4 py-4 border-0`}>
        <button className={`m-0 button-action`} onClick={handlerBidprice}>
          Kirim
        </button>
      </Modal.Footer>
    </Modal>
  );
}
