import React, { Fragment } from "react";
// import '../css/Card.css';
import {
  Button,
  Container,
  Row,
  Col,
  ToggleButton,
  ToggleButtonGroup,
} from "react-bootstrap";
import {
  Card,
  Grid,
  CardContent,
  Typography,
  CardMedia,
  // useMediaQuery
} from "@mui/material";

import ".././css/SellerProduct.css";
import { Icon } from "@mui/material";

import Navbar3 from "../component/Navbar3Icon";
import image1 from "../images/jam1.png";
import { Link, useNavigate, useParams } from "react-router-dom";
import { getUsers, users } from "../features/users/reducer";
import { getDaftarJual, data, getDiminati, getTerjual } from "../features/products/reducer";
import { useSelector, useDispatch } from "react-redux";
import { useState, useEffect } from 'react';
import { decodeToken } from "react-jwt";


let token = localStorage.getItem("access_token");
function SellerProduct() {
  const getId = decodeToken(token);
  let iduser = getId.id;
  console.log(iduser);
  const getUser = decodeToken(token);
  let user = getUser.name;
  const getImage = decodeToken(token);
  let image = getImage.image;
  const getCity = decodeToken(token);
  let city = getCity.city;
  console.log(city);
  // const user = getUser?.data?.users?.name;
  console.log(user);
  const {id}= useParams();
  const navigate = useNavigate();
  const history = useNavigate();
  let posts = useSelector(data);
  posts = posts?.data?.data?.products?.data;
  console.log(posts)
  const dispatch = useDispatch();
  const fetchData = () => {
    dispatch(getDaftarJual(id));
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Fragment>
      <Navbar3 />
      <Container style={{ marginTop: "20px" }}>
        <Row>
          <Col lg={12}>
            <div className="product-list-header">
              <div className="product-list-header-left">
                <div className="product-list-header-left-text">
                  <h3>Daftar Jual Saya</h3>
                </div>
              </div>
            </div>
            <div className="d-flex CardUser mb-3">
              <div
                className="d-flex justify-content-center align-items-center w-100"
                // onClick={() => {
                //   navigate("/myprofile");
                // }}
              >
                <img src={image} alt="" className="me-3 imgUser" />
                <div className="userInfoText">
                  <h5>{user}</h5>
                  <p className="m-0" style={{ fontSize: "14px" }}>
                  {city}
                  </p>
                </div>
              </div>
              <Button
                variant="outline"
                style={{
                  display: "flex",
                  justifyContent: "end",
                  borderRadius: "16px",
                  borderColor: "#7126B5",
                }}
                onClick={() => {
                  navigate("/infoprofile");
                }}
              >
                Edit
              </Button>
            </div>
          </Col>
        </Row>
        <Row>
          <Col lg={3} className="p-0 py-2">
            <div className="category-product-list">
              <ToggleButtonGroup
                type="radio"
                name="category-mobile-version"
                className="button-group-custom"
              >
                <h6 className="mb-3 text-category">Kategori</h6>
                <ToggleButton
                onClick={() => dispatch(getDaftarJual(id))}
                  id="radio-button-1"
                  value={"Hobi"}
                  className="btn-group-category bb-category"
                >
                  
                  <div className="d-flex justify-content-center align-items-center">
                    <Icon> view_in_ar </Icon>
                    <p className="m-0">Semua Produk</p>
                  </div>
                  <Icon>chevron_right</Icon>
                </ToggleButton>
                <ToggleButton
                  onClick={() => dispatch(getDiminati(id))}
                  id="radio-button-2"
                  value={"Hobi"}
                  className="btn-group-category bb-category"
                >
                  <div className="d-flex justify-content-center align-items-center">
                    <Icon>favorite_border</Icon>
                    <p className="m-0">Diminati</p>
                  </div>
                  <Icon>chevron_right</Icon>
                </ToggleButton>
                <ToggleButton
                  onClick={() => dispatch(getTerjual(id))}
                  id="radio-button-3"
                  value={"Kendaraan"}
                  className="btn-group-category"
                >
                  <div className="d-flex justify-content-center align-items-center">
                    <Icon> attach_money </Icon>
                    <p className="m-0">Terjual</p>
                  </div>
                  <Icon>chevron_right</Icon>
                </ToggleButton>
              </ToggleButtonGroup>
            </div>
          </Col>
          <Col lg={9} className="p-0">
            <Row className="m-auto">
              <Col xl="3" lg="4" md="4" sm="6" xs="6" className="my-2 px-2">
                <div
                  className="p-2 text-center text-secondary add-product-box"
                  onClick={() => {
                    navigate("/infoproduk");
                  }}
                >
                  <Icon>add</Icon>
                  <p>Tambah Produk</p>
                </div>
              </Col><div className="card-product">
                {posts && (
                  <Container sx={{border:'none'}}>
                <Grid container spacing={3}>
                {posts?.map((row, i) => (
                  <Grid key={i} item xs={2}>
                    <Card onClick = {()=>history(`/buyerproduk/${row.id}`)} style={{ height: "auto" }}>
                      <CardMedia
                        component="img"
                        src={row?.productImages?.[0].image}
                        style={{ height: "150px" }}
                      />
                      <CardContent>
                        <b>{row?.productName}</b>
                      </CardContent>
                      <CardContent>{row?.category}</CardContent>
                      <CardContent>Rp. {row?.price}</CardContent>
                    </Card>
                  </Grid>
                ))}
                {posts.status === "loading" && <Typography>Loading</Typography>}
                    </Grid>
              </Container>      
                )}

                </div>

              {/* <Col xl="3" lg="4" md="4" sm="6" xs="6" className="my-2 px-2">
                <Link to="/detailproduct"></Link>Get Produk Daftar Jual
              </Col>
              <Col xl="3" lg="4" md="4" sm="6" xs="6" className="my-2 px-2">
                <Link to="/detailproduct"></Link>Get Produk Daftar Jual
              </Col> */}
            </Row>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}

export default SellerProduct;
