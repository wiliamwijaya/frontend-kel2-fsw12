import * as React from "react";
import { Container } from "react-bootstrap";

import ".././css/AkunProfil.css";
import { useNavigate } from "react-router-dom";
import { usersLogout } from "../features/users/reducer";
import { IconButton, Box, Icon, Stack } from "@mui/material";

import { decodeToken } from "react-jwt";
import { styled } from "@mui/material/styles";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";

import PhotoCameraIcon from "@mui/icons-material/PhotoCamera";
import BottomNavigation from "../component/BottomNavigations";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { WindowSharp } from "@mui/icons-material";

const isLogin = localStorage.getItem("access_token");
const token = decodeToken(isLogin);
const MyProfile = () => {
  const Input = styled("input")({
    display: "none",
  });
  const navigate = useNavigate();
  const statusLogout = useSelector((state) => state.users.statusLogout);
  const dispatch = useDispatch();
  useEffect(() => {
    if (statusLogout) {
      navigate("/");
    }
  }, [statusLogout]);
  useEffect(() => {
    if (statusLogout) {
      alert("LogOut Berhasil");
      window.location.reload();
      localStorage.removeItem("access_token");
    }
  }, [statusLogout, navigate]);
  return (
    <>
      <Container
        fluid
        className="d-flex justify-content-center flex-column"
        style={{ height: "100vh", overflowX: "hidden" }}
      >
        <section
          style={{
            width: "100%",
            padding: "0 16px 100px",
          }}
        >
          <p className="ajk">Akun Saya</p>
          <div className="d-flex justify-content-center pb-5">
            <Stack direction="row" spacing={30}>
              <IconButton
                aria-label="arrow back"
                sx={{ ml: "-300px" }}
                onClick={() => {
                  navigate("/");
                }}
              >
                <ArrowBackIcon />
              </IconButton>
            </Stack>
            <Box
              component="span"
              sx={{
                p: 2,
                borderRadius: "12px",
                backgroundColor: "#E2D4F0",
              }}
            >
              <label htmlFor="icon-button-file">
                <IconButton aria-label="upload picture" component="span">
                  {token.image ? (
                    <img src={token.image} alt="" className="img" />
                  ) : (
                    <PhotoCameraIcon />
                  )}
                </IconButton>
              </label>
            </Box>
          </div>

          <div
            className="btn-holder"
            onClick={() => {
              navigate("/infoprofile");
            }}
          >
            <Icon sx={{ color: "#7126B5" }}>drive_file_rename_outline</Icon>
            <h6 className="ms-3 mb-0">Ubah Akun</h6>
          </div>

          <div className="btn-holder">
            <Icon sx={{ color: "#7126B5" }}>settings</Icon>
            <h6 className="ms-3 mb-0">Pengaturan Akun</h6>
          </div>

          <div className="btn-holder" onClick={() => dispatch(usersLogout())}>
            <Icon sx={{ color: "#7126B5" }}>logout</Icon>
            <h6 className="ms-3 mb-0">Keluar</h6>
          </div>

          <div className="version">
            <p className="version-text">Version 1.0.0</p>
          </div>
        </section>
      </Container>
      <BottomNavigation />
    </>
  );
};

export default MyProfile;
