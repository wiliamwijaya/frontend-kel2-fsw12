import { render, screen, cleanup } from "@testing-library/react";
import Register from "../Register";

test("Register Page Test", () => {
  render(<Register />);
  const registerPage = screen.getByRole("register");
  expect(registerPage).toBeInTheDocument();
});
