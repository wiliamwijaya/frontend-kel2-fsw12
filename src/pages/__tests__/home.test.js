import { render, screen, cleanup } from "@testing-library/react";
import Home from "../Home";

test("Home Page Test", () => {
  render(<Home />);
  const homePage = screen.getByRole("home");
  expect(homePage).toBeInTheDocument();
});
