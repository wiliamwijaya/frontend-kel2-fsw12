import { render, screen, cleanup } from "@testing-library/react";
import InfoProduk from "../InfoProduk";

test("Info Produk Page Test", () => {
  render(<InfoProduk />);
  const infoprodukPage = screen.getByRole("info-produk");
  expect(infoprodukPage).toBeInTheDocument();
});
