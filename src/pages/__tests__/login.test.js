import { render, screen, cleanup } from "@testing-library/react";
import Login from "../Login";

test("Login Page Test", () => {
  render(<Login />);
  const loginPage = screen.getByRole("login");
  expect(loginPage).toBeInTheDocument();
});
