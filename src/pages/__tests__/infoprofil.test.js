import { render, screen, cleanup } from "@testing-library/react";
import InfoProfil from "../InfoProfil";

test("Info Profil Page Test", () => {
  render(<InfoProfil />);
  const infoprofilPage = screen.getByRole("info-profil");
  expect(infoprofilPage).toBeInTheDocument();
});
