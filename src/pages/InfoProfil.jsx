import * as React from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Grid,
  Box,
  Stack,
} from "@mui/material";
import ".././css/Login.css";

import { useEffect } from "react";
import { Form, Button } from "react-bootstrap";

import { styled } from "@mui/material/styles";
import { decodeToken } from "react-jwt";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import PhotoCameraIcon from "@mui/icons-material/PhotoCamera";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { usersUpdate ,setError, data} from "../features/users/reducer";
let token = localStorage.getItem("access_token");
const InfoProfil = () => {

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [name, setName] = React.useState("");
  const [address, setAddress] = React.useState("");
  const [cit, setCity] = React.useState("");
  const [image, setImage] = React.useState(null);
  const [phoneNumber, setPhoneNumber] = React.useState("");
  const statusUpdate = useSelector((state) => state.users.statusUpdate);

  const id = decodeToken(token);
  let idimg = id.image;
  let iduser = id.id;
  const handleSubmit = (e) => {
    console.log(statusUpdate);
    e.preventDefault();
    console.log(name);
      if (name === "" || address === "" || cit === "" || phoneNumber === "") {
        dispatch(setError("Email dan Password tidak boleh kosong"));
      } else{
        let city;
        if(cit === "1"){
          city = "Jakarta"
        }else if(cit === "2"){
          city = "Bandung"
        }else if(cit === "3"){
          city = "Bogor"
        }
          const data = new FormData();
          data.append("name", name);
          data.append("address", address);
          data.append("city", city);
          data.append("image", image)
          data.append("phoneNumber", phoneNumber);
          dispatch(usersUpdate({iduser,data}));
      }
  };
  
  useEffect(() => {
    if (localStorage.getItem("access_token") === null) {
       navigate("/login");
    }
  })
  useEffect(() => {
       if(statusUpdate){
         navigate('/')
       }
     },[statusUpdate, navigate])
  useEffect(() => {
    if (statusUpdate) {
      window.location.reload();
      alert("Update berhasil");
    }
  }, [statusUpdate]);
 
  const Input = styled("input")({
    display: "none",
  });


  return (
    <div role="info-profil">
      <Grid
        container
        component="main"
        sx={{ height: "100vh", overflowX: "hidden" }}
      >
        <AppBar
          position="absolute"
          sx={{
            position: "relative",
            backgroundColor: "white",
            alignItems: "center",
          }}
        >
          <Toolbar>
            <IconButton
              edge="start"
              aria-label="arrow back"
              className="arroprofil"
              onClick={() => {
                navigate("/");
              }}
            >
              <ArrowBackIcon />
            </IconButton>
            <Typography
              className="titleprofile"
              variant="subtitle2"
              component="div"
              color="black"
            >
              Lengkapi Info Akun
            </Typography>
          </Toolbar>
        </AppBar>

        <Grid container sx={{ height: "100vh" }}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Box
              className="profil-box"
              sx={{
                mt: 2,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Stack direction="row" spacing={30}>
                <IconButton
                  aria-label="arrow back"
                  sx={{ ml: "-300px" }}
                  onClick={() => {
                    navigate("/");
                  }}
                >
                  <ArrowBackIcon />
                </IconButton>

                <Box
                  component="span"
                  sx={{
                    p: 2,
                    borderRadius: "12px",
                    backgroundColor: "#E2D4F0",
                  }}
                >
                  <label htmlFor="icon-button-file">
                    <Input id="icon-button-file"
                      type="file" 
                      onChange={(e) => setImage(e.target.files[0])}
                    />
                    <IconButton aria-label="upload picture" component="span">
                      {idimg?<img src={idimg} alt="" className="img"/>:<PhotoCameraIcon />}
                      
                    </IconButton>
                  </label>
                </Box>
              </Stack>

              <Box
                noValidate
                onSubmit={handleSubmit}
                sx={{ mt: 3, width: 400, height: 400 }}
              >
                <div className="forminfo">
                  <Form>
                    <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlInput1"
                    >
                      <Form.Label>Nama*</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder={id.name}
                        name="name"
                        onChange={(e) => setName(e.target.value)}
                        style={{
                          borderRadius: "16px",
                          fontSize: "14px",
                          fontFamily: "Poppins",
                        }}
                      />
                    </Form.Group>

                    <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlTextarea1"
                    >
                      <Form.Label>Alamat*</Form.Label>
                      <Form.Control
                      type="text"
                        as="textarea"
                        rows={3}
                        name="address"
                        value={address}
                        onChange={(e) => setAddress(e.target.value)}
                        placeholder={id.address}
                        style={{
                          borderRadius: "16px",
                          fontSize: "14px",
                          fontFamily: "Poppins",
                        }}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3">
                      <Form.Label>Kota*</Form.Label>
                      <Form.Select
                        aria-label="Default select example"
                        name="city"
                        onChange={(e) => setCity(e.target.value)}
                        style={{
                          borderRadius: "16px",
                          fontSize: "14px",
                          fontFamily: "Poppins",
                        }}
                      >
                        <option>Pilih Kota</option>
                        <option value="1">Jakarta</option>
                        <option value="2">Bandung</option>
                        <option value="3">Bogor</option>
                      </Form.Select>
                    </Form.Group>

                    <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlInput1"
                    >
                      <Form.Label>No Handphone*</Form.Label>
                      <Form.Control
                        type="number"
                        name="phoneNumber"
                        value={phoneNumber}
                        onChange={(e) => setPhoneNumber(e.target.value)}
                        placeholder={id.phoneNumber}

                        style={{
                          borderRadius: "16px",
                          fontSize: "14px",
                          fontFamily: "Poppins",
                        }}
                      />
                    </Form.Group>
                  </Form>

                  <div className="d-grid gap-2">
                    <Button
                      className="hrsf"
                      size="lg"
                      style={{
                        marginTop: "10px",
                        backgroundColor: "#7126B5",
                        borderRadius: "16px",
                        fontSize: "14px",
                      }}
                      onClick={handleSubmit}
                    >
                      Simpan
                    </Button>
                  </div>
                  <br />
                </div>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default InfoProfil;
