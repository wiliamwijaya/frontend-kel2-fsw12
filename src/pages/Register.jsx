import * as React from "react";
import ".././css/Login.css";
import {
  CssBaseline,
  Paper,
  Box,
  Grid,
  Typography,
  FormControl,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Tooltip,
} from "@mui/material";
import { Button } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';
import { Visibility, VisibilityOff } from "@mui/icons-material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import image from "../images/img.png";
import { useEffect } from "react";

import { usersRegister } from "../features/users/reducer";
import { useDispatch, useSelector } from "react-redux";
export default function Register() {
  const [values, setValues] = React.useState({
    password: "",
    showPassword: false,
  });

  const [email, setEmail] = React.useState("");
  const [name, setName] = React.useState("");
  const [password, setPassword] = React.useState("");
  
  const statusRegister = useSelector((state) => state.users.statusRegister);
  const dispatch = useDispatch();
  const Navigate = useNavigate();
  const handleregister = (e) => {
      e.preventDefault();
      console.log(name);
      if (name === "" || email === "" || values.password === "") {
        alert("Nama, Email dan Password tidak boleh kosong");
      } else{
          const data = {
            email,
            password,
            name,
          };
          dispatch(usersRegister(data));
          console.log(data);
      }
  }
  useEffect(() => {
    if (statusRegister) {
      Navigate("/login");
    }
  }, [statusRegister]);
  useEffect(() => {
    if (statusRegister) {
      window.location.reload();
      alert("Register berhasil");
    }
  }, [statusRegister, Navigate]);

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
    setPassword(event.target.value);
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    console.log({
      name: data.get("nama"),
      email: data.get("email"),
      password: data.get("password"),
    });
  };

  return (
    <div role="register">
      <Grid
        container
        component="main"
        sx={{ height: "100vh", overflowX: "hidden" }}
      >
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={false}
          md={7}
          lg={6}
          sx={{
            backgroundImage: `url(${image})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid
          item
          xs={12}
          sm={12}
          md={5}
          lg={6}
          component={Paper}
          elevation={6}
          square
        >
          <Tooltip title="left" placement="left-end">
            <IconButton aria-label="arrow back" className="arrow-back">
              <ArrowBackIcon />
            </IconButton>
          </Tooltip>
          <Box
            className="register-box"
            sx={{
              my: 5,
              mx: 5,
              px: 4,
              py: 4,
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Typography
              variant="h6"
              gutterBottom
              component="div"
              fontWeight="bold"
            >
              Daftar
            </Typography>
            <Box
              component="form"
              noValidate
              onSubmit={handleSubmit}
              sx={{ mt: 1 }}
            >
              <FormControl fullWidth sx={{ mb: 2 }}>
                <Typography variant="caption" display="block" gutterBottom>
                  Nama
                </Typography>
                <OutlinedInput
                  id="name"
                  size="small"
                  name="name"
                  label="Nama"
                  placeholder="Nama Lengkap"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  sx={{ borderRadius: "16px", fontSize: "14px" }}
                />
              </FormControl>

              <FormControl fullWidth sx={{ mb: 2 }}>
                <Typography variant="caption" display="block" gutterBottom>
                  Email
                </Typography>
                <OutlinedInput
                  id="email"
                  size="small"
                  name="email"
                  label="Email"
                  placeholder="Contoh: johndee@gmail.com"
                  
                  onChange={(e) => setEmail(e.target.value)}
                  sx={{ borderRadius: "16px", fontSize: "14px" }}
                />
              </FormControl>

              <FormControl fullWidth sx={{ mb: 2 }}>
                <Typography variant="caption" display="block" gutterBottom>
                  Password
                </Typography>
                <OutlinedInput
                  id="outlined-adornment-password"
                  name="password"
                  type={values.showPassword ? "text" : "password"}
                  placeholder="Password"
                  size="small"
                  value={values.password}
                  onChange={handleChange("password")}
                  sx={{ borderRadius: "16px", fontSize: "14px" }}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {values.showPassword ? (
                          <VisibilityOff />
                        ) : (
                          <Visibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                  label="Password"
                />
              </FormControl>

              <div className="d-grid gap-2">
                <Button
                  size="lg"
                  style={{
                    marginTop: "10px",
                    marginBottom: "10px",
                    backgroundColor: "#7126B5",
                    borderRadius: "16px",
                    fontSize: "14px",
                    fontFamily: "Poppins",
                  }}
                  onClick={handleregister}
                >
                  Daftar
                </Button>
              </div>
              <Grid container justifyContent="center" className="hrf">
                <Grid item>
                  <Typography variant="subtitle2" gutterBottom component="div">
                    Sudah punya akun?
                    <Button variant="body1" onClick={() => Navigate("/Login")}>
                      Login Disini
                    </Button>
                  </Typography>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}
