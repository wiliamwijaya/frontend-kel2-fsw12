import { configureStore } from "@reduxjs/toolkit";
import usersReducer from "../features/users/reducer";
import productReducer from "../features/products/reducer";

export const store = configureStore({
  reducer: {
    users: usersReducer,
    products: productReducer
  },
});
