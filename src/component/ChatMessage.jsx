import { useEffect, useState } from 'react'
import logo from '../logo.svg'
import io from 'socket.io-client'
import '../App.css'
import { Room } from '@mui/icons-material';

const socket = io.connect(process.env.REACT_APP_BACKEND_URL);

function ChatMessage ({socket, username, room}) {
  const [message, setMessage] = useState('')
  const [messages, setMessages] = useState([])

  const sendMessage = async() => {
    if(message !== ""){
        const messageData = {
            room: room,
            author: username,
            message: message,
    
        }
    }
  }
  
  function handleTextChange (e) {
    setMessage(e.target.value)
    console.log(message)
  }

  function handleSubmit (e) {
    e.preventDefault()
    if (!message || message === '') return

    socket.emit('chat message', message)
    setMessages([...messages, { msg: message, status: 0, time: new Date() }])
    setMessage('')
  }

  useEffect(() => {
    socket.on('incoming message', (message) => {
      console.log(message)
      setMessages([...messages, { msg: message, status: 1 }])
    })
  }, [socket, messages])


  return (
    <div className='App'>
      <img className="App-logo" src={logo} alt="react logo" />
      <div className="App-messages">
        {messages.map((message, index) => (
          <div className={message.status === 1 ? 'green ' : 'grey'} key={index}>
            {message.msg}
          </div>
        ))}
      </div>
      <form className="App-control" onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Message..."
          onChange={(e) => setMessage(e.target.value)}
          value={message}
        />
        <input className="App-button" type="submit" value="Send" />
      </form>
    </div>
  )
}

export default ChatMessage
