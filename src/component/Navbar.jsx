import * as React from "react";
import { useNavigate } from "react-router-dom";
import { Container, Navbar, Nav, Button, Offcanvas } from "react-bootstrap";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { styled, alpha } from '@mui/material/styles';
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import LoginIcon from "@mui/icons-material/Login";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/Navbar.css";

import { searchProduct } from "../features/products/reducer";
import { useSelector, useDispatch } from "react-redux";

const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [search, setSearch] = React.useState("");

  // const { productName } = useParams();

  // const handleSearch = (e) => {
  //   e.preventDefault();
  //   dispatch(searchProduct(search));
  // };

  return (
    <div role="navbar">
      {["md"].map((expand) => (
        <Navbar key={expand} expand={expand} className="ms-auto nav-shadow">
          <Container fluid>
            <Navbar.Brand
              onClick={() => navigate("/")}
              className="logo"
            ></Navbar.Brand>
            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-${expand}`}
              aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                  SecondHand
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body>
                <Paper
                  component="form"
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    width: "444px",
                    borderRadius: "10px",
                    backgroundColor: "#EEEEEE",
                    height: "40px",
                  }}
                >
                  <InputBase
                    onSubmit={(e) => {
                      e.preventDefault();
                      dispatch(searchProduct(search));
                    }}
                    onChange={(e) => setSearch(e.target.value)}
                    // onSubmit={handleSearch}
                    sx={{ ml: 1, flex: 1 }}
                    placeholder="Cari di sini..."
                  />

                  <IconButton
                    type="submit"
                    onClick={() => {
                      dispatch(searchProduct(search));
                      console.log(search);
                    }}
                    sx={{ p: "5px" }}
                    aria-label="search"
                  >
                    <SearchIcon />
                  </IconButton>
                </Paper>
                <Nav className="justify-content-end flex-grow-1 pe-5 menu">
                  <Button
                    className="header-button"
                    onClick={() => navigate("/Login")}
                  >
                    <IconButton
                      type="button"
                      sx={{ p: "2px", color: "#FFFFFF", height: "25px" }}
                      aria-label="search"
                    >
                      <LoginIcon />
                    </IconButton>
                    Masuk
                  </Button>
                </Nav>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
      ))}
    </div>
  );
};

export default Header;
