import * as React from "react";
import { Container, Navbar, Nav, Offcanvas } from "react-bootstrap"; //react-bootstrap
import {
  Paper,
  Popover,
  Typography,
  InputBase,
  Divider,
  IconButton,
  Grid,
  Card,
  CardContent,
  CardMedia,
} from "@mui/material"; //material ui
import SearchIcon from "@mui/icons-material/Search";
import DehazeOutlinedIcon from "@mui/icons-material/DehazeOutlined";
import NotificationsNoneOutlinedIcon from "@mui/icons-material/NotificationsNoneOutlined";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/Navbar.css";
import { useEffect } from "react";
import { getProducts, data, searchProduct } from "../features/products/reducer";
import { users } from "../features/users/reducer";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { useState } from "react";

import { decodeToken } from "react-jwt";

let token = localStorage.getItem("access_token");
const Header = () => {
  const getId = decodeToken(token);
  let iduser = getId.id;
  console.log(iduser);

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleNavigate = (event) => {
    navigate(`/infopenawar/${iduser}`);
  };

  const handleClose = () => {
    setAnchorEl(false);
  };

  // const {search} = use
  const dispatch = useDispatch();
  const fetchData = () => {
    dispatch(searchProduct());
  };

  useEffect(() => {
    fetchData();
  }, []);
  const {id} = useParams();
  const navigate = useNavigate();
  const [product_name, setName] = useState("");
  const open = Boolean(anchorEl);
  // const id = open ? "simple-popover" : undefined;
  // let getseller = useSelector(data);
  // getseller = getseller?.data?.data?.products?.data;
  let posts = useSelector(data);
  posts = posts?.data?.data?.products?.data;
  console.log(product_name);
  return (
    <>
      <div role="navbar3icon">
        {["md"].map((expand) => (
          <Navbar key={expand} expand={expand} className="ms-auto nav-shadow">
            <Container fluid>
              <Navbar.Brand
                onClick={() => navigate("/")}
                className="logo"
              ></Navbar.Brand>
              <Navbar.Toggle
                aria-controls={`offcanvasNavbar-expand-${expand}`}
              />
              <Navbar.Offcanvas
                id={`offcanvasNavbar-expand-${expand}`}
                aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
                placement="end"
              >
                <Offcanvas.Header closeButton>
                  <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                    SecondHand
                  </Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                  <Paper
                    component="form"
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      width: "444px",
                      borderRadius: "10px",
                      backgroundColor: "#EEEEEE",
                      height: "40px",
                    }}
                  >
                    <InputBase
                      onChange={(e) => setName(e.target.value)}
                      sx={{ ml: 1, flex: 1 }}
                      placeholder="Cari di sini..."
                      inputProps={{ "aria-label": "cari" }}
                    />
                    <IconButton
                      onClick={() => dispatch(searchProduct(product_name))}
                      sx={{ p: "5px" }}
                      aria-label="search"
                    >
                      <SearchIcon />
                    </IconButton>
                    <Divider
                      sx={{ height: 20, m: 0.5 }}
                      orientation="vertical"
                    />
                  </Paper>
                  <Nav className="justify-content-end flex-grow-1 pe-5 menu">
                    {/* {posts?.map((row, i) => ( */}
                    {/* {getseller?.map((row, i) => ( */}
                      <IconButton
                        onClick={() => navigate(`/sellerproduct/${iduser}`)}
                      >
                        <DehazeOutlinedIcon />
                      </IconButton>
                      
                    {/* ))} */}
                    <IconButton aria-label="Notification" onClick={handleClick}>
                      <NotificationsNoneOutlinedIcon />
                    </IconButton>
                    <Popover
                      id={id}
                      open={open}
                      anchorEl={anchorEl}
                      onClose={handleClose}
                      onClick={handleNavigate}
                      anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "center",
                      }}
                      transformOrigin={{
                        vertical: "top",
                        horizontal: "right",
                      }}
                    >
                      <div className="card-product">
                        {posts && (
                          <Container sx={{ border: "none" }}>
                            <Grid
                              container
                              spacing={3}
                              sx={{ flexDirection: "column" }}
                            >
                              {posts?.map((row, i) => (
                                <Grid key={i} item>
                                  <Card style={{ height: "auto" }}>
                                    <CardMedia
                                      component="img"
                                      src={row?.image}
                                      style={{ height: "50px" }}
                                    />
                                    <CardContent>
                                      <b>{row?.productName}</b>
                                    </CardContent>
                                    <CardContent>{row?.category}</CardContent>
                                    <CardContent>Rp. {row?.price}</CardContent>
                                  </Card>
                                </Grid>
                              ))}
                              {posts.status === "loading" && (
                                <Typography>Loading</Typography>
                              )}
                            </Grid>
                          </Container>
                        )}
                      </div>
                    </Popover>
                    <IconButton
                      onClick={() => {
                        navigate(`/akunprofil`);
                      }}
                    >
                      <PersonOutlineOutlinedIcon />
                      {/* <Popover
                      id={id}
                      open={open}
                      anchorEl={anchorEl}
                      onClose={handleClose}
                      anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "center",
                      }}
                      transformOrigin={{
                        vertical: "top",
                        horizontal: "right",
                      }}
                    >
                      <div className="card-product">
                        {posts && (
                          <Container sx={{ border: "none" }}>
                            <Grid container spacing={3}>
                              {posts?.map((row, i) => (
                                <Grid key={i} item xs={2}>
                                  <Card style={{ height: "auto" }}>
                                    <CardMedia
                                      component="img"
                                      src={row?.image}
                                      style={{ height: "50px" }}
                                    />
                                    <CardContent>
                                      <b>{row?.productName}</b>
                                    </CardContent>
                                    <CardContent>{row?.category}</CardContent>
                                    <CardContent>Rp. {row?.price}</CardContent>
                                  </Card>
                                </Grid>
                              ))}
                              {posts.status === "loading" && <Typography>Loading</Typography>}
                            </Grid>
                          </Container>
                        )}
                      </div>
                    </Popover> */}
                    </IconButton>
                  </Nav>
                </Offcanvas.Body>
              </Navbar.Offcanvas>
            </Container>
          </Navbar>
        ))}
      </div>
    </>
  );
};

export default Header;
