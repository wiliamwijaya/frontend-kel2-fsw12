import { render, screen, cleanup } from "@testing-library/react";
import Carousel from "../Carousel";

test("Carousel Component Test", () => {
  render(<Carousel />);
  const carouselElement = screen.getByRole("carousel");
  expect(carouselElement).toBeInTheDocument();
});
