import { render, screen, cleanup } from "@testing-library/react";
import Card from "../Card";

test("Card Component Test", () => {
  render(<Card />);
  const cardElement = screen.getByRole("card");
  expect(cardElement).toBeInTheDocument();
});
