import { render, screen, cleanup } from "@testing-library/react";
import Filter from "../Filter";

test("Filter Component Test", () => {
  render(<Filter />);
  const filterElement = screen.getByRole("filter");
  expect(filterElement).toBeInTheDocument();
});
