import { render, screen, cleanup } from "@testing-library/react";
import Navbar from "../Navbar";

test("Navbar Component Test", () => {
  render(<Navbar />);
  const navbarElement = screen.getByRole("navbar");
  expect(navbarElement).toBeInTheDocument();
});
