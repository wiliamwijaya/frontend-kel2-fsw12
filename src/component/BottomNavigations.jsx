import * as React from "react";
import { Icon } from "@mui/material";
import ".././css/BottomNavigations.css";
import { useNavigate } from "react-router-dom";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import NotificationsNoneOutlinedIcon from "@mui/icons-material/NotificationsNoneOutlined";
import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";
import FormatListBulletedOutlinedIcon from "@mui/icons-material/FormatListBulletedOutlined";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";

const BottomNavigation = () => {
  const navigate = useNavigate();
  const logo = [
    {
      icon: <HomeOutlinedIcon />,
      text: "Home",
      link: "/",
    },
    {
      icon: <NotificationsNoneOutlinedIcon />,
      text: "Notifikasi",
      link: "/offerlist",
    },
    {
      icon: <AddCircleOutlineOutlinedIcon />,
      text: "Jual",
      link: "/addproduct",
    },
    {
      icon: <FormatListBulletedOutlinedIcon />,
      text: "Daftar Jual",
      link: "/productlist",
    },
    {
      icon: <PersonOutlineOutlinedIcon />,
      text: "Akun",
      link: "/infoprofile",
    },
  ];

  return (
    <section className="bottom-navigation">
      {logo.map((item, i) => {
        return (
          <div
            key={i}
            className="mx-auto py-2 d-flex flex-column justify-content-center align-items-center text-center btn-menu"
            onClick={() => {
              navigate(item.link);
            }}
          >
            <Icon>{item.icon}</Icon>
            <h6 className=" m-0 text">{item.text}</h6>
          </div>
        );
      })}
    </section>
  );
};

export default BottomNavigation;
